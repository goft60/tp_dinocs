using System;

namespace Mesozoic
{
    public abstract class Dinosaur
    {
        protected static int DinosaurCreate;

        protected string name;
        protected static string specie;
        protected int age;

        public Dinosaur(string name, int age)
        {
            this.name = name;
            this.age = age;
            DinosaurCreate ++;
        }
        public int getTotalCreate()
        {
            return DinosaurCreate;
        }
        public string sayHello()
        {
            return string.Format("Je suis {0} le {1}, j'ai {2} ans.", this.name, Dinosaur.specie, this.age);
        }

        public string roar()
        {
            return "Grrr";
        }
        public string getName()
        {
            return this.name;
        }
        public string getSpecie()
        {
            return Dinosaur.specie;
        }
        public int getAge()
        {
            return this.age;
        }
        public void setName(string name)
        {
            this.name = name;
        }
        public void setSpecie(string specie)
        {
            Dinosaur.specie = specie;
        }
        public void setAge(int age)
        {
            this.age = age;
        }
        public string hug(Dinosaur dinosaur)
        {
            return string.Format("Je suis {0} et je fais un calin à {1}", this.name, dinosaur.getName());
        }
    }
}