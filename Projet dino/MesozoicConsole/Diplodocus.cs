using System;

namespace Mesozoic
{
    public class Diplodocus : Dinosaur {
        static Diplodocus(){
            specie = "Diplodocus";
        }

        public Diplodocus(string name, int age) : base(name,age)
        {
            
        }
    }
}