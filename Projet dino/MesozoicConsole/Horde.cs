using System;
using System.Collections.Generic;
using Mesozoic;

namespace MesozoicHorde
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs;

        public Horde()
        {
            this.dinosaurs = new List<Dinosaur>();
        }
        public void Add(Dinosaur dino)
        {
            dinosaurs.Add(dino);
        }
        public void Remove(Dinosaur dino)
        {
            dinosaurs.Remove(dino);
        }
        public int CountDino()
        {
            return dinosaurs.Count;
        }
        public string sayHello()
        {
            string Hello="";
            foreach (Dinosaur dino in dinosaurs)
            {
                Hello = string.Format("{0} \n\r {1}",Hello, dino.sayHello());
            }
            return Hello;
        }

    }
}