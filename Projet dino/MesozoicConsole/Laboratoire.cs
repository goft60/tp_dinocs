using System;
using Mesozoic;

namespace Mesozoic
{
    public class Laboratory
    {
        public static Dinosaur CreateDinosaur(string name,string specie)
        {
            return new Dinosaur(name,specie,0);
        }
    }
}