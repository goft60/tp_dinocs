﻿using System;
using System.Collections.Generic;
using Mesozoic;

namespace MesozoicConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            Dinosaur louis = new Dinosaur("Louis","Stegausaurus",12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur lalie = new Dinosaur("Lalie", "Triceratops", 20);
            Dinosaur franck = new Dinosaur("Franck", "Pteranodon", 30);

            List<Dinosaur> dinosaurs = new List<Dinosaur>();

            dinosaurs.Add(louis);
            dinosaurs.Add(nessie);
            dinosaurs.Add(lalie);
            dinosaurs.Add(franck);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.RemoveAt(1);

            Console.WriteLine(dinosaurs.Count);

            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }

            dinosaurs.Remove(louis);

            Console.WriteLine(dinosaurs.Count);
            foreach (Dinosaur dino in dinosaurs)
            {
                Console.WriteLine(dino.sayHello());
            }
        }
    }
}
