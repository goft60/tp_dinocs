using System;
using Xunit;
using Mesozoic;

namespace MesozoicDiplo
{
    public class DiploTest
    {
        [Fact]
        public void DiploHello()
        {
            Diplodocus nessie = new Diplodocus("Nessie", 11);

            Assert.Equal("Je suis Nessie le , j'ai 11 ans", nessie.SayHello());
        }
    }
}