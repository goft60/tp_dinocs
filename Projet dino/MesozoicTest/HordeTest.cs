using System;
using Xunit;
using System.Collections.Generic;

using Mesozoic;
using MesozoicHorde;

namespace MesozoicHordeTest
{
    public class TestHorde
    {
        [Fact]
        public void TestAddDinotoHorde()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Horde dinosaurs = new Horde();

            dinosaurs.Add(louis);

            Assert.Equal(1, dinosaurs.CountDino());
        }
        [Fact]
        public void TestHordeConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);

            Horde dinosaurs = new Horde();
            dinosaurs.Add(louis);
            Assert.Equal(" \n\r Je suis Louis le Stegausaurus, j'ai 12 ans.", dinosaurs.sayHello());
            dinosaurs.Add(nessie);
            Assert.Equal(" \n\r Je suis Louis le Stegausaurus, j'ai 12 ans. \n\r Je suis Nessie le Diplodocus, j'ai 11 ans.", dinosaurs.sayHello());
            dinosaurs.Remove(louis);
            Assert.Equal(" \n\r Je suis Nessie le Diplodocus, j'ai 11 ans.", dinosaurs.sayHello());
        }
    }
}