using System;
using Xunit;
using Mesozoic;

namespace MesozoicLaboTest
{
    public class TestLabo
    {
        [Fact]
        public void CreateDinosaurTest()
        {
        
            Dinosaur new_dino = Laboratory.CreateDinosaur("Mitri","Raptor");
            Assert.Equal(4, new_dino.getTotalCreate());
        }
    }
}